<?php


class Posts {
	private $description;
	private $image;
	private $price;
	private $itemName;
	private $userID;

	// When a user makes a post we use this constructor. Needs necessary information to construct. 
	public function __construct($price, $itemName, $userID) {
		$this->price = $price;
		$this->itemName = $itemName;
		$this->userID = $userID;
	}

	public function addDescription($description) {
		$this->description = $description;
	}

	public function addImage($image) {
		$this->image = $image;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getImage() {
		return $this->image;
	}

	public function getPrice() {
		return $this->price;
	}

	public function getItemName() {
		return $this->itemName;
	}

	public function getUserID() {
		return $this->userID;
	}
}


class Users {
	private $dOB;
	private $name;
	private $paymentInformation;
	private $userRating;
	private $passWord;
	private $emailAddress;

	//When we make our account we use this constructor
	public function __construct($name, $dOB, $passWord, $emailAddress) {
		$this->name = $name;
		$this->dOB = $dOB;
		$this->passWord = $passWord;
		$this->emailAddress = $emailAddress;
	}

	//Later on we can add this
	public function addPaymentInformation($paymentInformation) {
		$this->paymentInformation = $paymentInformation;
	}

	public function getDOB() {
		return $this->dOB;
	}

	public function getName() {
		return $this->name;
	}

	public function getPaymentInformation() {
		return $this->paymentInformation;
	}

	public function getUserRating() {
		return $this->userRating;
	}

	public function getEmailAddress() {
		return $this->getEmailAddress;
	}


}




 ?>